﻿using System.Diagnostics;

namespace ChocoUpdater.Providers
{
    internal class Winget : IProvider
    {
        public event EventHandler? UpdateDataReceived;
        protected virtual void OnUpdateDataReceived(string? e)
        {
            e = e?.Trim();
            // We should probably fix the encoding
            // But I don't want the lines that have these characters anyway
            if (e != null && !(e.StartsWith("â–ˆ") || e.StartsWith("â–’")))
            {
                string? trimmed = e.Trim('-')
                    .Trim('\\')
                    .Trim('/')
                    .Trim('|');

                if (!string.IsNullOrWhiteSpace(trimmed))
                {
                    UpdateDataReceived?.Invoke(this, new UpdateDataEventArgs(e!));
                }
            }
        }

        public static IEnumerable<Package> GetOutdatedPackages()
        {
            Process process = Process.Start(new ProcessStartInfo()
            {
                FileName = "winget",
                Arguments = "update --disable-interactivity --nowarn",
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                UseShellExecute = false,
            }) ?? throw new NullReferenceException("The winget process wouldn't start");

            _ = process.WaitForExit(5000);

            // Read the output
            string output = process.StandardOutput.ReadToEnd().TrimEnd();

            // Guess what process still returns 0 even on error?
            // For an error where there is no network connection at least, one or more of the lines will start with "Error"
            if (process.ExitCode != 0 || output.Split().Any(x => x.StartsWith("Error")))
            {
                throw new Exception($"The winget process returned {process.ExitCode}.\n\n{output}");
            }

            // "Name" is the first thing in the table, and the last line is not part of the table
            string retVal = output[output.IndexOf("Name")..output.Length];

            // Create packages with the data
            return CreatePackages(retVal);
        }

        public Process? UpdatePackages(IEnumerable<Package> packages)
        {
            if (packages.Any())
            {
                ProcessStartInfo startInfo = new()
                {
                    FileName = "winget",
                    Arguments = $"upgrade {string.Join(" ", packages.Select(x => x.ID))} --disable-interactivity",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                };
                Process process = new()
                {
                    StartInfo = startInfo,
                    EnableRaisingEvents = true
                };
                process.OutputDataReceived += (sender, args) => OnUpdateDataReceived(args?.Data);
                process.ErrorDataReceived += (sender, args) => OnUpdateDataReceived(args?.Data);
                OnUpdateDataReceived("Updating Winget packages...\n=============================");
                _ = process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                return process;
            }
            else
            {
                return null;
            }
        }

        private static List<Package> CreatePackages(string Input)
        {
            List<Package> returnValue = [];

            // Get table positions, we need even the ones we're not using so we know where a column ends
            int namePosition = 0;
            int IDPosition = Input.IndexOf("Id");
            int versionPosition = Input.IndexOf("Version");
            int availablePosition = Input.IndexOf("Available");
            int sourcePosition = Input.IndexOf("Source");
            int length = sourcePosition + 7;

            IEnumerable<string> packages = Input
                .Split('\n')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Skip(2)
                // Attempt to filter out anything that isn't part of the table.
                .Where(x =>
                    x.Length == length &&
                    x[IDPosition - 1] == ' ' &&
                    x[versionPosition - 1] == ' ' &&
                    x[availablePosition - 1] == ' ' &&
                    x[sourcePosition - 1] == ' '
                    )
                ;

            Chocolatey chocolatey = new();

            // Loop through each and create their packages
            foreach (string package in packages)
            {
                // Create and add the package
                returnValue.Add(
                    new Package(
                        package[namePosition..IDPosition].Trim(),
                        package[IDPosition..versionPosition].Trim(),
                        package[versionPosition..availablePosition].Trim(),
                        package[availablePosition..sourcePosition].Trim(),
                        false,
                        Package.Providers.Winget,
                        !(Settings.Default.CrossCheck && Chocolatey.CheckInstallList(package[namePosition..IDPosition].Trim()))
                        )
                    );
            }
            // Why are you reading this comment
            // This returns the returnValue, what more do you want?
            return returnValue;
        }
    }
}
