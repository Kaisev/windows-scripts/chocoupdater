﻿using System.Windows;
using System.Windows.Controls.Primitives;

namespace ChocoUpdater.Helpers
{
    class TextBoxHelper
    {
        public static readonly DependencyProperty ScrollToEndProperty = DependencyProperty.RegisterAttached(
            "ScrollToEnd",
            typeof(bool),
            typeof(TextBoxHelper),
            new PropertyMetadata(false, (obj, args) =>
            {
                if (obj is TextBoxBase tb)
                {
                    tb.TextChanged += (sender, eventargs) => ((TextBoxBase)sender).ScrollToEnd();
                }
            })
        );
        public static bool GetScrollToEnd(DependencyObject obj) => (bool)obj.GetValue(ScrollToEndProperty);
        public static void SetScrollToEnd(DependencyObject obj, bool value) => obj.SetValue(ScrollToEndProperty, value);
    }
}
