﻿$ErrorActionPreference = 'Stop'

Install-BinFile 'ChocoUpdater' 'ChocoUpdater.exe'

$DesktopShortcutPath = Join-Path ([System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonDesktopDirectory)) "ChocoUpdater.lnk"
$StartMenuShortcutPath = Join-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" "ChocoUpdater.lnk"

Install-ChocolateyShortcut -ShortcutFilePath $DesktopShortcutPath -TargetPath "ChocoUpdater.exe"
Install-ChocolateyShortcut -ShortcutFilePath $StartMenuShortcutPath -TargetPath "ChocoUpdater.exe"

$task = Get-ScheduledTask "ChocoUpdater" -ErrorAction SilentlyContinue

if ($null -eq $task) {
  Write-Host " Writing scheduled task `"ChocoUpdater`""
  $action = New-ScheduledTaskAction -Execute "ChocoUpdater.exe" -Argument "autoexit"
  $trigger = New-ScheduledTaskTrigger -AtLogon
  $principal = New-ScheduledTaskPrincipal -GroupId "BUILTIN\Administrators" -RunLevel Highest
  $settings = New-ScheduledTaskSettingsSet
  $task = New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger -Settings $settings -Description "Runs ChocoUpdater"
  Register-ScheduledTask ChocoUpdater -InputObject $task -Force
}
elseif ($task.Actions.Arguments -notlike "*autoexit") {
  Write-Host " `"ChocoUpdater`" scheduled task is missing the autoexit argument, adding"
  $task.Actions[0] = New-ScheduledTaskAction -Execute "ChocoUpdater.exe" -Argument "autoexit"
  Register-ScheduledTask ChocoUpdater -InputObject $task -Force
}
else {
  Write-Host " `"ChocoUpdater`" scheduled task already exists, skipping"
}