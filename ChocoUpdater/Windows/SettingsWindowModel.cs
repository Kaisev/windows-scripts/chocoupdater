﻿using System.ComponentModel;

namespace ChocoUpdater.Windows
{
    internal class SettingsWindowModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Sends a notification that a property in this class changed
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string Property)
        {
            Settings.Default.Save();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Property));
        }
        public bool Chocolatey
        {
            get => Settings.Default.Chocolatey;
            set
            {
                Settings.Default.Chocolatey = value;
                OnPropertyChanged(nameof(Chocolatey));
            }
        }
        public bool Winget
        {
            get => Settings.Default.Winget;
            set
            {
                Settings.Default.Winget = value;
                OnPropertyChanged(nameof(Winget));
            }
        }
        public bool CrossCheck
        {
            get => Settings.Default.CrossCheck;
            set
            {
                Settings.Default.CrossCheck = value;
                OnPropertyChanged(nameof(CrossCheck));
            }
        }
    }
}
