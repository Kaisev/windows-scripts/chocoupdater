﻿using System.Diagnostics;
using System.Windows;

namespace ChocoUpdater.Providers
{
    internal class Chocolatey : IProvider
    {
        public event EventHandler? UpdateDataReceived;
        protected virtual void OnUpdateDataReceived(string? e)
        {
            if (e != null)
            {
                UpdateDataReceived?.Invoke(this, new UpdateDataEventArgs(e));
            }
        }

        public static IEnumerable<string> InstalledApps
        {
            get
            {
                Process process = Process.Start(new ProcessStartInfo()
                {
                    FileName = "choco",
                    Arguments = "list -r",
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                }) ?? throw new NullReferenceException("The choco process wouldn't start");

                _ = process.WaitForExit(5000);

                // Read the output
                string output = process.StandardOutput.ReadToEnd();

                return output.Split('\r').Select(x => x.Split('|')[0].Trim()).Where(x => !string.IsNullOrWhiteSpace(x));
            }
        }

        /// <summary>
        /// Gets a collection of Chocolatey packages that have available updates
        /// </summary>
        /// <returns>The packages that have available updates</returns>
        /// <exception cref="NullReferenceException">Occurs when the Chocolatey process ends up being null</exception>
        /// <exception cref="Exception">An otherwise unhandled exception</exception>
        public static IEnumerable<Package> GetOutdatedPackages()
        {
            Process process = Process.Start(new ProcessStartInfo()
            {
                FileName = "choco",
                Arguments = "outdated -r",
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                UseShellExecute = false,
            }) ?? throw new NullReferenceException("The choco process wouldn't start");

            _ = process.WaitForExit(5000);

            // Read the output
            string output = process.StandardOutput.ReadToEnd();

            // Guess what process still returns 0 even on error?
            // For an error where there is no network connection at least, one or more of the lines will start with "Error"
            if (process.ExitCode != 0 || output.Split().Any(x => x.StartsWith("Error")))
            {
                throw new Exception($"The choco process returned {process.ExitCode}.\n\n{output}");
            }

            // Create packages with the data
            return CreatePackages(output);
        }

        /// <summary>
        /// Issue the command to update the given packages
        /// </summary>
        /// <param name="packages">The collection of packages to update</param>
        public Process? UpdatePackages(IEnumerable<Package> packages)
        {
            if (packages.Any())
            {
                ProcessStartInfo startInfo = new()
                {
                    FileName = "choco",
                    Arguments = $"upgrade {string.Join(" ", packages.Where(x => x.Name != "chocoupdater").Select(x => x.Name))} -y --no-progress -r",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                };
                Process process = new()
                {
                    StartInfo = startInfo,
                    EnableRaisingEvents = true,
                };
                process.OutputDataReceived += (sender, args) => OnUpdateDataReceived(args?.Data);
                process.ErrorDataReceived += (sender, args) => OnUpdateDataReceived(args?.Data);
                OnUpdateDataReceived("Updating Chocolatey packages...\n=============================");
                _ = process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                return process;
            }
            else
            {
                return null;
            }
        }

        public static void SelfUpdate()
        {
            // This is a bit of a hack and not a nice user experience, but it works to allow the application to update itself
            _ = Process.Start("choco", "upgrade chocoupdater -y --no-progress -r");
            Application.Current.Dispatcher.Invoke(Application.Current.Shutdown);
        }

        public static bool CheckInstallList(string package) => InstalledApps.Any(x => x.Contains(package, StringComparison.CurrentCultureIgnoreCase) || package.Contains(x, StringComparison.CurrentCultureIgnoreCase));

        /// <summary>
        /// Creates a list of packages from the output of choco outdated
        /// </summary>
        /// <param name="Input">The text from the choco outdated -r command. DOES NOT support choco outdated without the -r</param>
        /// <returns>A list of parsed packages</returns>
        private static IEnumerable<Package> CreatePackages(string Input)
        {
            List<Package> returnValue = [];
            // Split the rows into their own strings. Remove the empty lines
            IEnumerable<string> packages = Input.Split().Where(x => !string.IsNullOrWhiteSpace(x));

            // Loop through each and create their packages
            foreach (string package in packages)
            {
                // Split out each parameter
                string[] parsedPackage = package.Split("|").Select(x => x.Trim()).ToArray();

                // Create and add the package
                returnValue.Add(
                    new Package(
                        parsedPackage[0],
                        parsedPackage[0],
                        parsedPackage[1],
                        parsedPackage[2],
                        bool.Parse(parsedPackage[3]),
                        Package.Providers.Chocolatey
                        )
                    );
            }
            // Why are you reading this comment
            // This returns the returnValue, what more do you want?
            return returnValue.Where(x => !x.Pinned);
        }
    }
}
