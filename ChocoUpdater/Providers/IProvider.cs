﻿using System.Diagnostics;

namespace ChocoUpdater.Providers
{
    internal interface IProvider
    {
        abstract public static IEnumerable<Package> GetOutdatedPackages();
        abstract public Process? UpdatePackages(IEnumerable<Package> packages);
        public abstract event EventHandler UpdateDataReceived;
    }
    public class UpdateDataEventArgs(string message) : EventArgs
    {
        public string Message = message;
    }
}
