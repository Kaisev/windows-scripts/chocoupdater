# ChocoUpdater

This is a simple tool to check for and prompt a user with Chocolatey and Winget updates.

## Releases

The latest release is available from Chocolatey:  
[https://community.chocolatey.org/packages/chocoupdater]() 

The latest version of ChocoUpdater.exe is also available on the [Releases](https://gitlab.com/Kaisev/windows-scripts/chocoupdater/-/releases) page

## Demo

![Demo Video](ChocoUpdater_Demo.webm)  
<sub>Desktop background <a href="//www.davidrevoy.com/article736/speedpainting">*Speedpainting* by David Revoy</a>. Licensed under <a href="//creativecommons.org/licenses/by/4.0">CC BY</a></sub>

## Setup

Install Chocoupdater by running `choco install chocoupdater`

A scheduled task named `chocoupdater` will be created with a trigger to run at the logon of any user. This task only runs as users in the local Administrators group, as this access is necessary for updating Chocolatey packages.

Additional triggers can be defined as desired. Note that updates to Chocoupdater will not modify your task, but uninstalling Chocoupdater will delete the scheduled task.

## Contribution

To contribute, please make a fork of the project, make your changes, and submit a merge request.
