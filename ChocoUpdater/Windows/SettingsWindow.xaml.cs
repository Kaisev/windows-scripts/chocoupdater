﻿using System.Windows;
using ChocoUpdater.Windows;

namespace ChocoUpdater
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {

        internal SettingsWindowModel Model { get; set; }
        public SettingsWindow()
        {
            InitializeComponent();
            Model = new();
            DataContext = Model;
        }
        // This is to deal with a known bug
        // https://github.com/Kinnara/ModernWpf/issues/142
        private void Window_Activated(object sender, EventArgs e) => InvalidateMeasure();
    }
}
