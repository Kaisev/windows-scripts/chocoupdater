﻿using System.ComponentModel;

namespace ChocoUpdater
{
    /// <summary>
    /// Create an instance of a package
    /// </summary>
    /// <param name="name">The name of the package</param>
    /// <param name="version">The current version of the package</param>
    /// <param name="versionAvailable">The latest available version available</param>
    /// <param name="pinned">Whether or not the application is pinned. I'm not actually sure what that means, and I don't care</param>
    /// <param name="provider">The provider source for the package</param>
    internal class Package(string name, string id, string version, string versionAvailable, bool pinned, Package.Providers provider, bool install = true) : INotifyPropertyChanged
    {
        public enum Providers
        {
            Chocolatey,
            Winget
        }
        /// <summary>
        /// A list of packages that are outdated
        /// </summary>
        public static List<Package> OutDatedPackages { get; set; } = [];
        /// <summary>
        /// The name of the package
        /// </summary>
        public string Name { get; private set; } = name;
        /// <summary>
        /// A unique Id used to represent the package
        /// </summary>
        public string ID { get; private set; } = id;
        /// <summary>
        /// The current version of the package
        /// </summary>
        public string Version { get; private set; } = version;
        /// <summary>
        /// The latest available version of the package
        /// </summary>
        public string VersionAvailable { get; private set; } = versionAvailable;
        /// <summary>
        /// Whether or not the application is pinned.
        /// It's in the output so I included it, but I have no interested in this
        /// </summary>
        public bool Pinned { get; private set; } = pinned;
        /// <summary>
        /// The provider for the package 
        /// </summary>
        public Providers Provider { get; private set; } = provider;

        private bool _Install = install;
        /// <summary>
        /// Whether or not to install the update
        /// </summary>
        public bool Install
        {
            get => _Install;
            set
            {
                _Install = value;
                OnPropertyChanged(nameof(Install));
            }
        }

        /// <summary>
        /// Sends a notification that a property in this class changed
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string Property) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Property));
    }
}
