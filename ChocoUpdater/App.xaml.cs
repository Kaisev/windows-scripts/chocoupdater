﻿using System.Windows;

namespace ChocoUpdater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App() => InitializeComponent();

        [STAThread]
        private void Application_Startup(object sender, StartupEventArgs e) => _ = new UpdateWindow().ShowDialog();
    }
}
