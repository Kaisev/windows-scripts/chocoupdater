﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using ChocoUpdater.Providers;

namespace ChocoUpdater.Windows
{
    internal class UpdateWindowModel : INotifyPropertyChanged
    {
        internal UpdateWindowModel() => GetUpdates();

        private void X_PropertyChanged(object? sender, PropertyChangedEventArgs e) => OnPropertyChanged(nameof(SelectAll));

        private void Packages_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => OnPropertyChanged(nameof(SelectAll));

        /// <summary>
        /// Sends a notification that a property in this class changed
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string Property) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Property));
        public ObservableCollection<Package> Packages { get; set; } = [];
        public bool SelectAll
        {
            get => Packages.All(x => x.Install);
            set => Packages.ToList().ForEach(x => x.Install = value);
        }

        private bool _loading;
        public bool Loading
        {
            get => _loading;
            set
            {
                _loading = value;
                OnPropertyChanged(nameof(Loading));
                OnPropertyChanged(nameof(NoneFound));
                OnPropertyChanged(nameof(HasUpdates));
            }
        }
        private bool _installing = false;
        public bool Installing
        {
            get => _installing;
            set
            {
                _installing = value;
                OnPropertyChanged(nameof(Installing));
                OnPropertyChanged(nameof(NoneFound));
                OnPropertyChanged(nameof(HasUpdates));
            }
        }
        public bool NoneFound => !Installing && !Loading && !HasUpdates;
        public bool HasUpdates => !Installing && !Loading && Packages.Any();
        private bool _hasUpdateOutput;
        public bool HasUpdateOutput
        {
            get => _hasUpdateOutput;
            set
            {
                _hasUpdateOutput = value;
                OnPropertyChanged(nameof(HasUpdateOutput));
            }
        }
        public static bool AutoExit
        {
            get
            {
                string[] args = Environment.GetCommandLineArgs();
                return args.Length > 1 && args[1].Equals("autoexit", StringComparison.CurrentCultureIgnoreCase);
            }
        }

        private string _updateOutput = "";
        public string UpdateOutput
        {
            get => _updateOutput;
            set
            {
                _updateOutput = value;
                OnPropertyChanged(nameof(UpdateOutput));
            }
        }

        public async void GetUpdates()
        {
            Loading = true;
            if (Settings.Default.Chocolatey)
            {
                // Create packages with the data
                await Task.Run(() =>
                {
                    foreach (Package package in Chocolatey.GetOutdatedPackages())
                    {
                        // This might not work, or might not even do anything
                        package.PropertyChanged += X_PropertyChanged;
                        Application.Current.Dispatcher.Invoke(() => Packages.Add(package));
                    }

                    OnPropertyChanged(nameof(Packages));
                });
            }

            if (Settings.Default.Winget)
            {
                await Task.Run(() =>
                {
                    foreach (Package package in Winget.GetOutdatedPackages())
                    {
                        // This might not work, or might not even do anything
                        package.PropertyChanged += X_PropertyChanged;
                        Application.Current.Dispatcher.Invoke(() => Packages.Add(package));
                    }

                    OnPropertyChanged(nameof(Packages));
                });
            }

            Loading = false;
            if (AutoExit && NoneFound)
            {
                Application.Current.Shutdown();
            }
        }
        public async void Update()
        {
            Installing = true;
            Winget winget = new();
            winget.UpdateDataReceived += Provider_UpdateDataReceived;
            Process? wingetProcess = winget.UpdatePackages(Packages.Where(x => x.Install).Where(x => x.Provider == Package.Providers.Winget));
            if (wingetProcess != null)
            {
                await wingetProcess.WaitForExitAsync();
            }

            Chocolatey choco = new();
            choco.UpdateDataReceived += Provider_UpdateDataReceived;
            Process? chocoProcess = choco.UpdatePackages(Packages.Where(x => x.Install).Where(x => x.Provider == Package.Providers.Chocolatey));
            if (chocoProcess != null)
            {
                await chocoProcess.WaitForExitAsync();
            }

            // This method will exit the application if autoexit is checked
            await UpdateCompleteAsync();

            if (Packages.Where(x => x.Name == "chocoupdater").Any())
            {
                Chocolatey.SelfUpdate();
            }

            Installing = false;
            Application.Current.Dispatcher.Invoke(Packages.Clear);
            GetUpdates();
        }

        private void Provider_UpdateDataReceived(object? sender, EventArgs e)
        {
            HasUpdateOutput = true;
            UpdateOutput += "\n" + ((UpdateDataEventArgs)e).Message;
        }
        private Task UpdateCompleteAsync()
        {
            return Task.Run(() =>
            {
                Thread.Sleep(2000); // gives the user time to register what's happening
                if (AutoExit)
                {
                    Application.Current.Dispatcher.Invoke(() => Application.Current.Shutdown());
                }
            });
        }
    }
}
