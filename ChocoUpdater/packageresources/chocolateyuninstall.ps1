﻿$ErrorActionPreference = 'Stop'

$DesktopShortcutPath = Join-Path ([System.Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonDesktopDirectory)) "ChocoUpdater.lnk"
$StartMenuShortcutPath = Join-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" "ChocoUpdater.lnk"

Remove-Item $DesktopShortcutPath -ErrorAction SilentlyContinue
Remove-Item $StartMenuShortcutPath -ErrorAction SilentlyContinue

if (Get-ScheduledTask "ChocoUpdater" -ErrorAction SilentlyContinue) {
  Write-Host " Removing scheduled task"
  Unregister-ScheduledTask ChocoUpdater -Confirm:$false
}

Uninstall-BinFile 'ChocoUpdater'