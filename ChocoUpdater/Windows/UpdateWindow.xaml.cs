﻿using System.Windows;
using ChocoUpdater.Windows;

namespace ChocoUpdater
{
    /// <summary>
    /// Interaction logic for UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        internal UpdateWindowModel Model { get; set; }
        public UpdateWindow()
        {
            InitializeComponent();
            Model = new();
            DataContext = Model;
        }
        private void Yes_Click(object sender, RoutedEventArgs e) => Model.Update();

        private void No_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown();

        // This is to deal with a known bug
        // https://github.com/Kinnara/ModernWpf/issues/142
        private void Window_Activated(object sender, EventArgs e) => InvalidateMeasure();

        private void Settings_Click(object sender, RoutedEventArgs e) => new SettingsWindow().ShowDialog();

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Model.Packages.Clear();
            Model.GetUpdates();
        }
    }
}
